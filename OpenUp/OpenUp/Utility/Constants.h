//
//  Constants.h
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#define USER_TOKEN_DEFAULTS @"userToken"
#define MOOD_UPDATE_DATE_DEFAULTS @"moodLastUpdatedOn"


//=============Error Codes ============//
#define ERROR_CODE_INVALID_USER 1001


#define PRIMARY_COLOR @"#FF9800"
#define SECONDARY_COLOR @"#F57C00"
#define PROGRESS_INDICATOR_COLOR @"#000000"
#define LIST_DIVIDER_COLOR @"#979797"
#define MENU_TEXT_COLOR @"#979797"
#define DARK_TEXT_COLOR @"#000000"
#define PRIMARY_TEXT_COLOR @"#FFFFFF"
#define TRANSPARENT_COLOR @"#00FFFFFF"