//
//  Utils.m
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "Utils.h"
#import "Constants.h"
#import "NSDate+DateTools.h"
@implementation Utils
+(void)writeToDefaultsForKey:(NSString*)key andValue:(NSString*)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:value forKey:key];
    [defaults synchronize];
}

+(void)writeFloatToDefaultsForKey:(NSString*)key andValue:(float)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:value forKey:key];
    [defaults synchronize];
}

+(float)readFloatValueFromDefaultsForKey:(NSString*)key
{
    return [[NSUserDefaults standardUserDefaults] floatForKey:key];
}


+(NSString*)readValueFromDefaultsForKey:(NSString*)key
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
}

// takes @"#123456"
+ (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    return [self colorWithHex:x];
}

// takes 0x123456
+ (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

+(BOOL)isMoodUpdatedToday
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[self readFloatValueFromDefaultsForKey:MOOD_UPDATE_DATE_DEFAULTS]];
    
    return [date isToday];
}

+(void)updateMoodDateInDefaults
{
    [self writeFloatToDefaultsForKey:MOOD_UPDATE_DATE_DEFAULTS andValue:[[NSDate date] timeIntervalSince1970]];
}
@end
