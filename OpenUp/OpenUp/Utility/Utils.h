//
//  Utils.h
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Utils : NSObject
{
    
}
+(void)writeToDefaultsForKey:(NSString*)key andValue:(NSString*)value;
+(NSString*)readValueFromDefaultsForKey:(NSString*)key;
+ (BOOL)validateEmailWithString:(NSString*)email;
+ (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message;
+ (UIColor *)colorWithHexString:(NSString *)str;
+ (UIColor *)colorWithHex:(UInt32)col;
+(BOOL)isMoodUpdatedToday;
+(void)updateMoodDateInDefaults;

@end
