//
//  AppDelegate.h
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,REFrostedViewControllerDelegate>

@property (strong,nonatomic) REFrostedViewController *frostedViewController;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

-(void)setUpHomeViewController;
-(void)showMenu;
-(void)hideMenu;
@end

