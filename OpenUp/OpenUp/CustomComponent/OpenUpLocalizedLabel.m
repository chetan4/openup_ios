//
//  OpenUpLocalizedLabel.m
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "OpenUpLocalizedLabel.h"

@implementation OpenUpLocalizedLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setText:(NSString *)text
{
    [super setText:NSLocalizedString(text, "")];
}

-(void)awakeFromNib
{
    [super setText:NSLocalizedString([self text], "")];
}
@end
