//
//  FeedItem.h
//  OpenUp
//
//  Created by Javal Nanda on 19/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedItem : UIView
{
    IBOutlet UILabel *messageLabel;
    IBOutlet UILabel *timeStampLabel;
}
-(void)setData:(NSDictionary*)feedData;
@end
