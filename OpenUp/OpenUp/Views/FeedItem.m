//
//  FeedItem.m
//  OpenUp
//
//  Created by Javal Nanda on 19/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "FeedItem.h"
#import "NSDate+DateTools.h"
@implementation FeedItem

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setData:(NSDictionary*)feedData
{
    messageLabel.text = [feedData valueForKey:@"content"];
    
    NSString *dateStr = [feedData valueForKey:@"created_at"];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter1 dateFromString:dateStr];
    NSLog(@"date : %@",date);
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date];
    

//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
//    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
//    [dateFormatter setTimeZone:timeZone];
//    NSDate *date = [dateFormatter dateFromString:[feedData valueForKey:@"created_at"]];

    timeStampLabel.text = destinationDate.shortTimeAgoSinceNow;
}

@end
