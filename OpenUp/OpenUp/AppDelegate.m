//
//  AppDelegate.m
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "AppDelegate.h"
#import "Utils.h"
#import "Constants.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"
#import "SideMenuViewController.h"
//#import "IQKeyboardManager.h"
#import "MoodViewController.h"
#import "NSDate+DateTools.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize frostedViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    application.applicationSupportsShakeToEdit = YES;

//    [[IQKeyboardManager sharedManager] disableInViewControllerClass:[HomeViewController class]];

    UIViewController *rootViewController;
    if([Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS]
       && [Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS].length>0)
    {
        [self setUpHomeViewController];
    }
    else
    {
        
        rootViewController = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
        self.navigationController = [[UINavigationController alloc]initWithRootViewController:rootViewController];
        self.window.rootViewController = self.navigationController;
        [[UINavigationBar appearance]setBarTintColor:[Utils colorWithHexString:PRIMARY_COLOR]];
        self.navigationController.navigationBarHidden = TRUE;
    }
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound|UIUserNotificationTypeBadge
                                                                                                              categories:nil]];
    }
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [[NSDate date] dateByAddingDays:1];
    localNotification.alertBody = @"Checkout what your colleagues are posting!!";
    localNotification.timeZone = [NSTimeZone localTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if (![Utils isMoodUpdatedToday])
    {
        NSLog(@"%@",self.window.rootViewController);
        UIViewController *rootViewController = self.window.rootViewController;
        if ([rootViewController isKindOfClass:[REFrostedViewController class]])
        {
            UIViewController *contentViewController = ((UINavigationController*)((REFrostedViewController*)rootViewController).contentViewController).visibleViewController;
            if ([contentViewController isKindOfClass:[HomeViewController class]])
            {
                [((HomeViewController*)contentViewController) showMoodScreen];
            }
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)showMoodScreen
{
    MoodViewController *moodViewController = [[MoodViewController alloc]initWithNibName:@"MoodViewController" bundle:nil];
    self.window.rootViewController = moodViewController;
}

-(void)setUpHomeViewController
{
    // Create content and menu controllers
    //
    
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    self.navigationController = [[UINavigationController alloc]initWithRootViewController:homeViewController];
    [[UINavigationBar appearance]setBarTintColor:[Utils colorWithHexString:SECONDARY_COLOR]];
    SideMenuViewController *menuController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController" bundle:nil];
    
    // Create frosted view controller
    //
    self.frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:self.navigationController menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    frostedViewController.liveBlur = YES;
    frostedViewController.delegate = self;
    
    // Make it a root controller
    //
    self.window.rootViewController = frostedViewController;
}

-(void)showMenu
{
    // Dismiss keyboard (optional)
    //
//    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];

}

-(void)hideMenu
{
    
}
@end
