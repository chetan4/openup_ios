//
//  OUURLs.m
//  OpenUp
//
//  Created by Javal Nanda on 15/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "OUURLs.h"

@implementation OUURLs

NSString *const REGISTER_USER = CURRENT_BASE_URL @"users";
NSString *const VERIFY_USER = CURRENT_BASE_URL @"users/verify";
NSString *const POST_STATUS = CURRENT_BASE_URL @"posts";
NSString *const GET_FEED = CURRENT_BASE_URL @"posts";
NSString *const UPDATE_MOOD = CURRENT_BASE_URL @"users/update_mood";
NSString *const UPVOTE = CURRENT_BASE_URL @"posts/postid/upvote";
NSString *const DOWNVOTE = CURRENT_BASE_URL @"posts/postid/downvote";
NSString *const FLAG = CURRENT_BASE_URL @"posts/postid/flag";
NSString *const INVITE = CURRENT_BASE_URL @"users/invite";

NSString *const COMMUNITY_STANDARDS = CURRENT_HTML_URL @"community_standards";
NSString *const PRIVACY = CURRENT_HTML_URL @"privacy";
NSString *const TERMS_OF_SERVICE = CURRENT_HTML_URL @"terms_of_services";
NSString *const ABOUT_US = CURRENT_HTML_URL @"about";
@end
