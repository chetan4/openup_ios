//
//  OUURLs.h
//  OpenUp
//
//  Created by Javal Nanda on 15/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <Foundation/Foundation.h>


#define BASE_STAGING_URL @"http://openupapi.herokuapp.com/api/v1/"
#define BASE_PRODUCTION_URL @"http://openupapi.herokuapp.com/"
#define CURRENT_BASE_URL BASE_STAGING_URL

#define BASE_HTML_RESOURCE_URL_PROD @"http://openupapi.herokuapp.com/"
#define BASE_HTML_RESOURCE_URL_STAG @"http://openupapi.herokuapp.com/"
#define CURRENT_HTML_URL BASE_HTML_RESOURCE_URL_STAG


@interface OUURLs : NSObject

FOUNDATION_EXPORT NSString *const REGISTER_USER;
FOUNDATION_EXPORT NSString *const VERIFY_USER;
FOUNDATION_EXPORT NSString *const POST_STATUS;
FOUNDATION_EXPORT NSString *const GET_FEED;
FOUNDATION_EXPORT NSString *const UPDATE_MOOD;
FOUNDATION_EXPORT NSString *const UPVOTE;
FOUNDATION_EXPORT NSString *const DOWNVOTE;
FOUNDATION_EXPORT NSString *const FLAG;
FOUNDATION_EXPORT NSString *const INVITE;

FOUNDATION_EXPORT NSString *const COMMUNITY_STANDARDS;
FOUNDATION_EXPORT NSString *const PRIVACY;
FOUNDATION_EXPORT NSString *const TERMS_OF_SERVICE;
FOUNDATION_EXPORT NSString *const ABOUT_US;


@end
