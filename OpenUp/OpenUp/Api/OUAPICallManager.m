//
//  OUAPICallManager.m
//  OpenUp
//
//  Created by Javal Nanda on 15/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "OUAPICallManager.h"
#import "OUURLs.h"
#import "OUHttpUtils.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Utils.h"
#import "Constants.h"

@implementation OUAPICallManager

+ (id)manager{
//    OUAPICallManager *manager = [[OUAPICallManager alloc]init];
//    return manager;
//    
    static OUAPICallManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[OUAPICallManager alloc] init];
    });
    
    return sharedManager;
}


- (void)registerUser:(NSString*)email WithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"email":email};
    [OUHttpUtils executePostWithUrl:REGISTER_USER andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)verifyPinWithEmail:(NSString*)email andPin:(NSString*)pin WithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"email":email,@"pin":pin};
    [OUHttpUtils executePutWithUrl:VERIFY_USER andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)postStatus:(NSString*)message WithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS],@"content":message};
    [OUHttpUtils executePostWithUrl:POST_STATUS andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)fetchFeedwithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS],@"page":[NSString stringWithFormat:@"%d",_currentPageForFeed]};
    [OUHttpUtils executeGetWithUrl:GET_FEED andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)sendInviteToEmail:(NSString*)email withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS],@"email":email};
    [OUHttpUtils executePostWithUrl:INVITE andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)updateMood:(int)moodValue withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS],@"mood":[NSString stringWithFormat:@"%d",moodValue]};
    [OUHttpUtils executePostWithUrl:UPDATE_MOOD andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)upVotePost:(int)postId withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS]};
    [OUHttpUtils executePostWithUrl:[UPVOTE stringByReplacingOccurrencesOfString:@"postid" withString:[NSString stringWithFormat:@"%d",postId]] andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)downVotePost:(int)postId withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS]};
    [OUHttpUtils executePostWithUrl:[DOWNVOTE stringByReplacingOccurrencesOfString:@"postid" withString:[NSString stringWithFormat:@"%d",postId]] andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

- (void)flagPost:(int)postId withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView{
    NSDictionary *headers = @{};
    NSDictionary *parameters = @{@"user_token":[Utils readValueFromDefaultsForKey:USER_TOKEN_DEFAULTS]};
    [OUHttpUtils executePostWithUrl:[FLAG stringByReplacingOccurrencesOfString:@"postid" withString:[NSString stringWithFormat:@"%d",postId]] andParameters:parameters andHeaders:headers withSuccessHandler:success withFailureHandler:failure withLoadingViewOn:parentView];
}

//=========== Data manager methods =============//
- (void)setFeedArray:(NSMutableArray*)feedArray
{
    _feedArray = feedArray;
}

- (NSMutableArray*)getFeedArray
{
    return _feedArray;
}

- (void)setCurrentPageForFeed:(int)currentPage
{
    _currentPageForFeed = currentPage;
}

- (int)getCurrentPageOfFeed
{
    return _currentPageForFeed;
}

- (void)setTotalPageForFeed:(int)totalPage
{
    _totalPageForFeed = totalPage;
}

- (int)getTotalPageOfFeed
{
    return _totalPageForFeed;
}
@end
