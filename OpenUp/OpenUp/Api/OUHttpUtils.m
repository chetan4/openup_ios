//
//  OUHttpUtils.m
//  OpenUp
//
//  Created by Javal Nanda on 15/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "OUHttpUtils.h"
#import "Constants.h"
#import "Utils.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"

@implementation OUHttpUtils

+ (void) executeDeleteWithUrl:(NSString *)url AndParameters:(NSDictionary *)parameters AndHeaders:(NSDictionary *)headers withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure withLoadingViewOn:(UIView *)parentView {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // check if custom headers are present and add them
    if (headers != nil) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [[manager requestSerializer] setValue:obj forHTTPHeaderField:key];
        }];
    }
    if (parentView == nil) {
        [manager DELETE:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            bool apiSuccess = @YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }     failure:failure];
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:parentView animated:YES];
        [hud setLabelText:@"Loading"];
        [hud setLabelFont:[UIFont fontWithName:@"TitilliumWeb-SemiBold" size:18]];
        [manager DELETE:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            bool apiSuccess = @YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            NSLog(@"failure %@ - %@", operation, error);
            failure(operation, error);
        }];
    }
}

+ (void)executePostWithUrl:(NSString *)url andParameters:(NSDictionary *)parameters andHeaders:(NSDictionary *)headers constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure withLoadingViewOn:(UIView *)parentView {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // hack to allow 'text/plain' content-type to work
    NSMutableSet *contentTypes = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [contentTypes addObject:@"text/plain"];
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    // check if custom headers are present and add them
    if (headers != nil) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [[manager requestSerializer] setValue:obj forHTTPHeaderField:key];
        }];
    }
    if (parentView == nil) {
        [manager POST:url parameters:parameters constructingBodyWithBlock:block success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            bool apiSuccess = [[responseObject objectForKey:@"success"] isEqual:@YES];
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }     failure:failure];
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:parentView animated:YES];
        [hud setLabelText:@"Loading"];
        [hud setLabelFont:[UIFont fontWithName:@"TitilliumWeb-SemiBold" size:18]];
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            bool apiSuccess = [[responseObject objectForKey:@"success"] isEqual:@YES];
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            NSLog(@"failure %@ - %@", operation, error);
            failure(operation, error);
        }];
    }
}

+ (void)executePostWithUrl:(NSString *)url andParameters:(NSDictionary *)parameters andHeaders:(NSDictionary *)headers withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure withLoadingViewOn:(UIView *)parentView {
    NSLog(@"Executing post request for %@",url);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // check if custom headers are present and add them
    if (headers != nil) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [[manager requestSerializer] setValue:obj forHTTPHeaderField:key];
        }];
    }
    if (parentView == nil) {
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            bool apiSuccess = [[responseObject objectForKey:@"success"] isEqual:@YES];
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }     failure:failure];
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:parentView animated:YES];
        [hud setLabelText:@"Loading"];
        [hud setLabelFont:[UIFont fontWithName:@"TitilliumWeb-SemiBold" size:18]];
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            bool apiSuccess = @YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"failure %@ - %@", operation, error);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
           
            failure(operation, error);
        }];
    }
}

+ (void)executePutWithUrl:(NSString *)url andParameters:(NSDictionary *)parameters andHeaders:(NSDictionary *)headers withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure withLoadingViewOn:(UIView *)parentView {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // check if custom headers are present and add them
    if (headers != nil) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [[manager requestSerializer] setValue:obj forHTTPHeaderField:key];
        }];
    }
    if (parentView == nil) {
        [manager PUT:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            bool apiSuccess =@YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }    failure:failure];
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:parentView animated:YES];
        [hud setLabelText:@"Loading"];
        [hud setLabelFont:[UIFont fontWithName:@"TitilliumWeb-SemiBold" size:18]];
        [manager PUT:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            bool apiSuccess = [[responseObject objectForKey:@"success"] isEqual:@YES];
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            NSLog(@"failure %@ - %@", operation, error);
            failure(operation, error);
        }];
    }
}


+ (void)executeGetWithUrl:(NSString *)url andParameters:(NSDictionary *)parameters andHeaders:(NSDictionary *)headers withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure withLoadingViewOn:(UIView *)parentView {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // check if custom headers are present and add them
    if (headers != nil) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [[manager requestSerializer] setValue:obj forHTTPHeaderField:key];
        }];
    }
    // if failure is nil then its an indication to handle it with default flow that is showing message
    // in alert dialog
    
    if (parentView == nil) {
        [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            bool apiSuccess = @YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }    failure:failure];
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:parentView animated:YES];
        [hud setLabelText:@"Loading"];
        [hud setLabelFont:[UIFont fontWithName:@"TitilliumWeb-SemiBold" size:18]];
        [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"success %@ - %@", operation, responseObject);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            bool apiSuccess = @YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"failure %@ - %@", operation, error);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            failure(operation, error);
        }];
    }
}

+ (void)executePatchWithUrl:(NSString *)url andParameters:(NSDictionary *)parameters andHeaders:(NSDictionary *)headers withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure withLoadingViewOn:(UIView *)parentView {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // check if custom headers are present and add them
    if (headers != nil) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [[manager requestSerializer] setValue:obj forHTTPHeaderField:key];
        }];
    }
    if (parentView == nil) {
        [manager PATCH:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            bool apiSuccess = [[responseObject objectForKey:@"success"] isEqual:@YES];
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];
        }    failure:failure];
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:parentView animated:YES];
        [hud setLabelText:@"Loading"];
        [hud setLabelFont:[UIFont fontWithName:@"TitilliumWeb-SemiBold" size:18]];
        [manager PATCH:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"success %@ - %@", operation, responseObject);
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            bool apiSuccess = @YES;
//            success(operation, responseObject, apiSuccess);
            [self handleResponseForOperation:operation responseObject:responseObject apiSuccess:apiSuccess withSuccessHandler:success withFailureHandler:failure];            
        }    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:parentView animated:YES];
            NSLog(@"failure %@ - %@", operation, error);
            failure(operation, error);
        }];
    }
}

+(void)handleResponseForOperation:(AFHTTPRequestOperation *)operation responseObject:(id)response apiSuccess:(bool)apiSuccess withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success withFailureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    if (apiSuccess) {
        if ([[response valueForKey:@"success"] intValue]==1)
        {
            success(operation,response,apiSuccess);
        }
        else
        {
            NSString *errorMessage = [[response valueForKey:@"error"] valueForKey:@"data"];
            int errorCode = [[[response valueForKey:@"error"] valueForKey:@"error_code"] intValue];
            if (errorCode == ERROR_CODE_INVALID_USER) {
                //Remove user token
                [Utils writeToDefaultsForKey:USER_TOKEN_DEFAULTS andValue:@""];
                
                AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                
                //Redirect to register screen
                RegisterViewController *rootViewController = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
                appDel.navigationController = [[UINavigationController alloc]initWithRootViewController:rootViewController];
                appDel.window.rootViewController = appDel.navigationController;
                [[UINavigationBar appearance]setBarTintColor:[Utils colorWithHexString:PRIMARY_COLOR]];
                appDel.navigationController.navigationBarHidden = TRUE;
                
                [Utils showAlertWithTitle:NSLocalizedString(@"appName", nil) andMessage:errorMessage];
            }
            else
            {
                [Utils showAlertWithTitle:NSLocalizedString(@"appName", nil) andMessage:errorMessage];
            }
            
            failure(operation,nil);
        }
    }
    else
    {
            failure(operation,nil);
    }
}

@end
