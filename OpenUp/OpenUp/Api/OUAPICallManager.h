//
//  OUAPICallManager.h
//  OpenUp
//
//  Created by Javal Nanda on 15/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface OUAPICallManager : NSObject
{
    NSMutableArray *_feedArray;
    int _currentPageForFeed,_totalPageForFeed;
}

+ (id)manager;

- (void)registerUser:(NSString*)email WithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)verifyPinWithEmail:(NSString*)email andPin:(NSString*)pin WithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)fetchFeedwithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)postStatus:(NSString*)message WithSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)sendInviteToEmail:(NSString*)email withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)updateMood:(int)moodValue withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)upVotePost:(int)postId withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)downVotePost:(int)postId withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;
- (void)flagPost:(int)postId withSuccessHandler:(void (^)(AFHTTPRequestOperation *, id, bool))success failureHandler:(void (^)(AFHTTPRequestOperation *, NSError *))failure andLoadingViewOn:(UIView *)parentView;


//=========== Data manager methods =============//
- (void)setFeedArray:(NSMutableArray*)feedArray;
- (NSMutableArray*)getFeedArray;
- (void)setCurrentPageForFeed:(int)currentPage;
- (int)getCurrentPageOfFeed;
- (void)setTotalPageForFeed:(int)totalPage;
- (int)getTotalPageOfFeed;


@end
