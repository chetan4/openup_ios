//
//  VerifyPinViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "VerifyPinViewController.h"
#import "OUAPICallManager.h"
#import "Utils.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface VerifyPinViewController ()

@end

@implementation VerifyPinViewController
@synthesize emailToVerify;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    switch (newString.length)
    {
        case 0:
        {
            pin0.text = @"0";
            pin1.text = @"0";
            pin2.text = @"0";
            pin3.text = @"0";
        }
            break;
        case 1:
        {
            pin0.text = [newString substringWithRange:NSMakeRange(0, 1)];
            pin1.text = @"";
            pin2.text = @"";
            pin3.text = @"";
        }
            break;
        
        case 2:
        {
            pin0.text = [newString substringWithRange:NSMakeRange(0, 1)];
            pin1.text = [newString substringWithRange:NSMakeRange(1, 1)];
            pin2.text = @"";
            pin3.text = @"";
        }
            break;
            
        case 3:
        {
            pin0.text = [newString substringWithRange:NSMakeRange(0, 1)];
            pin1.text = [newString substringWithRange:NSMakeRange(1, 1)];
            pin2.text = [newString substringWithRange:NSMakeRange(2, 1)];
            pin3.text = @"";
        }
            break;

        case 4:
        {
            pin0.text = [newString substringWithRange:NSMakeRange(0, 1)];
            pin1.text = [newString substringWithRange:NSMakeRange(1, 1)];
            pin2.text = [newString substringWithRange:NSMakeRange(2, 1)];
            pin3.text = [newString substringWithRange:NSMakeRange(3, 1)];
        }
            break;

        default:
            break;
    }
    
    return YES;
}

-(IBAction)verifyClicked:(id)sender
{
    if ([pinTextField.text length]==4)
    {
        [self.view endEditing:TRUE];
        [[OUAPICallManager manager] verifyPinWithEmail:emailToVerify andPin:pinTextField.text WithSuccessHandler:^(AFHTTPRequestOperation *operation, id response, bool apiSuccess) {
            if (apiSuccess)
            {
                if ([response valueForKey:@"user_token"])
                {
                    [Utils writeToDefaultsForKey:USER_TOKEN_DEFAULTS andValue:[response valueForKey:@"user_token"]];
                    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    [appDel setUpHomeViewController];
                }
            }
        } failureHandler:^(AFHTTPRequestOperation *opeartion, NSError *error) {
            
        } andLoadingViewOn:self.view];
    }
    else
    {
        [Utils showAlertWithTitle:NSLocalizedString(@"appName", nil) andMessage:NSLocalizedString(@"enter_pin", nil)];
    }
}
@end
