//
//  RegisterViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController
{
    IBOutlet UITextField *emailTextField;
}

-(IBAction)registerClicked:(id)sender;
@end
