//
//  HomeViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 18/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "BaseViewController.h"
#import "PHPageScrollView.h"
#import "LPlaceholderTextView.h"

@interface HomeViewController : BaseViewController<PHPageScrollViewDataSource,PHPageScrollViewDelegate>
{
    IBOutlet UIButton *upVoteButton;
    IBOutlet UIButton *downVoteButton;
    IBOutlet UIButton *flagButton;
    
    IBOutlet UILabel *upVoteCount;
    IBOutlet UILabel *downVoteCount;
    
    IBOutlet PHPageScrollView *feedScrollView;
    
    IBOutlet UIImageView *leftArrow;
    IBOutlet UIImageView *rightArrow;
    NSMutableArray *feedArray;
    int currentFeed;
    IBOutlet UIView *composeView;
    IBOutlet UIView *feedView;
    IBOutlet LPlaceholderTextView *postTextView;
    IBOutlet UILabel *countLabel;
    
    UIImageView *navImageView;
}

- (void)toggleComposeVisibility;
- (IBAction)upVoteClicked:(id)sender;
- (IBAction)downVoteClicked:(id)sender;
- (IBAction)flagClicked:(id)sender;
- (IBAction)postClicked:(id)sender;

-(void)showMoodScreen;
@end
