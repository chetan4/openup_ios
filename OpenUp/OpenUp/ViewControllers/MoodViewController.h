//
//  MoodViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 28/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoodViewController : UIViewController
{
    IBOutlet UISlider *moodSlider;
    IBOutlet UIImageView *smileyImage;
    IBOutlet UIButton *proceedButton;
    int currentMoodValue;
}
-(IBAction)proceedClicked:(id)sender;
@end
