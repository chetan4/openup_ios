//
//  InviteViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 27/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface InviteViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    IBOutlet UITableView *contactsTable;
    IBOutlet UITextField *emailTextField;
    IBOutlet UISearchBar *searchBar;
    NSArray *contactsArray;
    NSArray *searchResults;
}
-(IBAction)inviteClicked:(id)sender;
@end
