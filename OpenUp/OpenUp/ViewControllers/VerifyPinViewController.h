//
//  VerifyPinViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VerifyPinViewController : UIViewController
{
    IBOutlet UITextField *pinTextField;
    IBOutlet UILabel *pin0, *pin1, *pin2, *pin3;
}
@property (nonatomic) NSString* emailToVerify;
-(IBAction)verifyClicked:(id)sender;
@end
