//
//  InviteViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 27/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "InviteViewController.h"
#import "APAddressBook.h"
#import "APContact.h"
#import "Utils.h"
#import "OUAPICallManager.h"

@interface InviteViewController ()

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"INVITE PEOPLE";
    [self fetchContacts];
    // Do any additional setup after loading the view from its nib.
}

-(void)fetchContacts
{
    APAddressBook *addressBook = [[APAddressBook alloc] init];
    // don't forget to show some activity
    addressBook.fieldsMask = APContactFieldFirstName | APContactFieldEmails;
    [addressBook loadContacts:^(NSArray *contacts, NSError *error)
     {
         // hide activity
         if (!error)
         {
             contactsArray = contacts;
             [contactsTable reloadData];
             // do something with contacts array
         }
         else
         {
             // show error
         }
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (searchResults && searchResults.count>0)?searchResults.count:contactsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    //
    //    CGRect frame = CGRectMake(50, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height);
    
    APContact *contact = [(searchResults && searchResults.count>0)?searchResults:contactsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [self contactName:contact];
    cell.detailTextLabel.text = [self contactEmails:contact];
    
    //    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    //    label.textColor = [UIColor lightGrayColor];
    //    label.text = [menuItems objectAtIndex:indexPath.row];
    //    [cell.contentView addSubview:label];
    
    return cell;
}

#pragma mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath{
    
    emailTextField.text = [self contactEmails:[(searchResults && searchResults.count>0)?searchResults:contactsArray objectAtIndex:indexPath.row]];
    [searchBar resignFirstResponder];
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{

    NSMutableArray *filteredArr = [NSMutableArray arrayWithCapacity:0];
    for (int i=0; i < contactsArray.count; i++)
    {
        APContact *contact = [contactsArray objectAtIndex:i];
        NSString *name = [self contactName:contact];
        NSString *email = [self contactEmails:contact];
        if ([name rangeOfString:searchText].location!=NSNotFound || [email rangeOfString:searchText].location!=NSNotFound)
        {
            [filteredArr addObject:contact];
        }
    }
    searchResults = filteredArr;
    [contactsTable reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    {
        [self filterContentForSearchText:searchText
                                   scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                          objectAtIndex:[self.searchDisplayController.searchBar
                                                         selectedScopeButtonIndex]]];
        
    }
}

- (NSString *)contactName:(APContact *)contact
{
    if (contact.compositeName)
    {
        return contact.compositeName;
    }
    else if (contact.firstName && contact.lastName)
    {
        return [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
    }
    else if (contact.firstName || contact.lastName)
    {
        return contact.firstName ?: contact.lastName;
    }
    else
    {
        return @"";
    }
}

- (NSString *)contactEmails:(APContact *)contact
{
    if (contact.emails.count > 1)
    {
        //        return [contact.emails componentsJoinedByString:@", "];
        return contact.emails.firstObject;
    }
    else
    {
        return contact.emails.firstObject ?: @"(No emails)";
    }
}

-(IBAction)inviteClicked:(id)sender
{
    if ([Utils validateEmailWithString:emailTextField.text])
    {
        [[OUAPICallManager manager] sendInviteToEmail:emailTextField.text withSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSucces)
        {
            if (apiSucces) {
                emailTextField.text = @"";
                [self.view endEditing:TRUE];
                [Utils showAlertWithTitle:NSLocalizedString(@"appName", nil)  andMessage:NSLocalizedString(@"invite_success", nil)];
            }
        } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error)
        {
            
        } andLoadingViewOn:self.view];
        
    }
    else
    {
        [Utils showAlertWithTitle:NSLocalizedString(@"appName", nil) andMessage:NSLocalizedString(@"enter_valid_email", nil)];
    }
}

@end
