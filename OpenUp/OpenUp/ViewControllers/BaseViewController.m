//
//  BaseViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 18/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "BaseViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "Constants.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *leftNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftNavButton setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    [leftNavButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [leftNavButton setFrame:CGRectMake(0, 0, 27, 17)];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftNavButton];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showMenu
{
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel showMenu];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
