//
//  HomeViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 18/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "HomeViewController.h"
#import "FeedItem.h"
#import "OUAPICallManager.h"
#import "MMTweenAnimation.h"
#import "MoodViewController.h"
#import "Utils.h"

#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentFeed = 0;
    self.title = @"";
    navImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"compose_button"]];
    navImageView.autoresizingMask = UIViewAutoresizingNone;
    navImageView.contentMode = UIViewContentModeCenter;
    
    UIButton *rightNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [rightNavButton setImage:[UIImage imageNamed:@"compose_button"] forState:UIControlStateNormal];
    [rightNavButton addTarget:self action:@selector(toggleComposeVisibility) forControlEvents:UIControlEventTouchUpInside];
    [rightNavButton setFrame:CGRectMake(0, 0, 24, 24)];
    [rightNavButton addSubview:navImageView];
    navImageView.center = rightNavButton.center;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightNavButton];
    self.navigationItem.rightBarButtonItem = rightItem;
    feedArray = [[OUAPICallManager manager] getFeedArray];
    feedScrollView.delegate = self;
    feedScrollView.dataSource = self;
    
    postTextView.placeholderColor = [UIColor lightGrayColor];
    postTextView.placeholderText = @"Say something nice..";
    
    if (feedArray && [feedArray count]>0)
    {
        [feedScrollView reloadData];
    }
    else
    {
        [self loadFeed];
    }
    
    // Do any additional setup after loading the view from its nib.
}


-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = FALSE;

    if (![Utils isMoodUpdatedToday])
    {
        [self showMoodScreen];
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
       [self becomeFirstResponder];
    if (feedArray.count>0)
    {
        [self updateUI:[feedArray objectAtIndex:currentFeed]];
    }

}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[OUAPICallManager manager] setCurrentPageForFeed:1];
        [feedScrollView scrollToPage:0 animation:TRUE];
        [self loadFeed];
    }
}

-(void)showMoodScreen
{
    self.navigationController.navigationBarHidden = TRUE;
    MoodViewController *moodController = [[MoodViewController alloc]initWithNibName:@"MoodViewController" bundle:nil];
    [self presentViewController:moodController animated:TRUE completion:^{
        
    }];
}
-(void)loadFeed
{
//    [self clearScrollViewComponents];

        [[OUAPICallManager manager] setCurrentPageForFeed:1];
        [[OUAPICallManager manager] fetchFeedwithSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess) {
            if (apiSuccess)
            {
                feedArray = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"feed"]];
                int totalPage = [[[responseObject objectForKey:@"page"] valueForKey:@"total_page"] intValue];
                [[OUAPICallManager manager] setFeedArray:feedArray];
                [[OUAPICallManager manager] setTotalPageForFeed:totalPage];
                [feedScrollView reloadData];
                if(feedArray.count>0)
                {
                    [self updateUI:[feedArray objectAtIndex:currentFeed]];
                }

            }
        } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } andLoadingViewOn:self.view];
}

-(void)loadNextPage
{
        [[OUAPICallManager manager] setCurrentPageForFeed:[[OUAPICallManager manager] getCurrentPageOfFeed]+1];
        [[OUAPICallManager manager] fetchFeedwithSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess) {
            if (apiSuccess)
            {
                [feedArray addObjectsFromArray:[responseObject objectForKey:@"feed"]] ;
                int totalPage = [[[responseObject objectForKey:@"page"] valueForKey:@"total_page"] intValue];
                [[OUAPICallManager manager] setFeedArray:feedArray];
                [[OUAPICallManager manager] setTotalPageForFeed:totalPage];
                [feedScrollView reloadData];
                [self updateUI:[feedArray objectAtIndex:currentFeed]];
            }
        } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } andLoadingViewOn:self.view];
}

-(void)updateUI:(NSDictionary*)feedData
{
    [upVoteButton setSelected:[[feedData valueForKey:@"user_upvoted"] intValue]==1?true:false];
    [downVoteButton setSelected:[[feedData valueForKey:@"user_downvoted"]intValue]==1?true:false];
    [flagButton setSelected:[[feedData valueForKey:@"user_flagged"] intValue]==1?true:false];
    
    [upVoteCount setText:[NSString stringWithFormat:@"%d",[[feedData valueForKey:@"upvote_count"] intValue]]];
    [downVoteCount setText:[NSString stringWithFormat:@"%d",[[feedData valueForKey:@"downvote_count"] intValue]]];
    
    if (feedArray.count==0)
    {
        leftArrow.hidden = TRUE;
        rightArrow.hidden = TRUE;
    }
    else if (currentFeed==0 && feedArray.count>1)
    {
        leftArrow.hidden = TRUE;
        rightArrow.hidden = FALSE;
    }
    else if (currentFeed>0 && currentFeed< feedArray.count-1)
    {
        leftArrow.hidden = FALSE;
        rightArrow.hidden = FALSE;
    }
    else if (currentFeed>0 && currentFeed == feedArray.count-1)
    {
        leftArrow.hidden = FALSE;
        rightArrow.hidden = TRUE;
    }
}
/*
- (void)reloadScrollViewContent
{
    for (int i=0; i < [feedArray count]; i++)
    {
        CGRect frame = CGRectMake(i*feedScrollView.frame.size.width, 0, feedScrollView.frame.size.width, feedScrollView.frame.size.height);
        NSArray *elementsInNib = [[NSBundle mainBundle] loadNibNamed:@"FeedItem" owner:Nil options:nil];
        FeedItem *feedItem = [elementsInNib lastObject];
        [feedItem setFrame:frame];
        [feedItem setData:[feedArray objectAtIndex:i]];
        [feedScrollView addSubview:feedItem];
    }
    [feedScrollView setContentSize:CGSizeMake(feedScrollView.frame.size.width*[feedArray count], feedScrollView.frame.size.height)];
}


-(void)clearScrollViewComponents
{
    for (UIView *view in feedScrollView.subviews)
    {
        [view removeFromSuperview];
    }
}
*/
#pragma mark -

- (NSInteger)numberOfPageInPageScrollView:(PHPageScrollView*)pageScrollView
{
    return feedArray.count;
}

- (CGSize)sizeCellForPageScrollView:(PHPageScrollView*)pageScrollView
{
    return CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height);
}

- (UIView*)pageScrollView:(PHPageScrollView*)pageScrollView viewForRowAtIndex:(int)index
{
    NSArray *elementsInNib = [[NSBundle mainBundle] loadNibNamed:@"FeedItem" owner:Nil options:nil];
    FeedItem *feedItem = [elementsInNib lastObject];
    [feedItem setFrame:CGRectMake(0, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
    [feedItem setData:[feedArray objectAtIndex:index]];
    
    return feedItem;
}

- (void)pageScrollView:(PHPageScrollView*)pageScrollView didScrollToPageAtIndex:(NSInteger)index
{
    currentFeed = (int)index;
    if(index==feedArray.count-1 && [[OUAPICallManager manager] getCurrentPageOfFeed]<[[OUAPICallManager manager] getTotalPageOfFeed])
    {
        [self loadNextPage];
    }
        [self updateUI:[feedArray objectAtIndex:index]];
}

- (void)pageScrollView:(PHPageScrollView *)pageScrollView didTapPageAtIndex:(NSInteger)index
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleComposeVisibility
{
//    [composeView pop_removeAllAnimations];
    if (composeView.hidden)
    {
        postTextView.text = @"";
        countLabel.text = @"140";
        composeView.hidden = FALSE;
        MMTweenAnimation *anim = [MMTweenAnimation animation];
        anim.functionType   = MMTweenFunctionElastic;
        anim.easingType     = MMTweenEasingOut;
        anim.duration       = 1.2f;
        anim.fromValue      = @[@(-composeView.frame.size.height/2)];
        anim.toValue        = @[@(64+composeView.frame.size.height/2)];
        anim.animationBlock = ^(double c,double d,NSArray *v,id target,MMTweenAnimation *animation)
        {
            double value = [v[0] doubleValue];
            composeView.center = CGPointMake(composeView.center.x, value);
        };
        
        [composeView pop_addAnimation:anim forKey:@"center.y"];


        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            navImageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
        } completion:^(BOOL finished) {
        }];
        

    }
    else
    {
        [self.view endEditing:TRUE];
        MMTweenAnimation *anim = [MMTweenAnimation animation];
        anim.functionType   = MMTweenFunctionElastic;
        anim.easingType     = MMTweenEasingIn;
        anim.duration       = 1.2f;
        anim.fromValue      = @[@(composeView.center.y)];
        anim.toValue        = @[@(-200-composeView.frame.size.height/2)];
        anim.animationBlock = ^(double c,double d,NSArray *v,id target,MMTweenAnimation *animation)
        {
            double value = [v[0] doubleValue];
            composeView.center = CGPointMake(composeView.center.x, value);
        };
        
        int64_t delay = 2; // In seconds
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
        dispatch_after(time, dispatch_get_main_queue(), ^(void){
            
                composeView.hidden = TRUE;
        });

        [composeView pop_addAnimation:anim forKey:@"center.y"];
        


        
        
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            navImageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0));
        } completion:^(BOOL finished) {
        }];
    }

}

#pragma mark button events

- (IBAction)upVoteClicked:(id)sender
{
        [[OUAPICallManager manager] upVotePost:[[[feedArray objectAtIndex:currentFeed] valueForKey:@"post_id"] intValue] withSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess) {
            if (apiSuccess) {
                [self refreshCurrentFeed:[responseObject objectForKey:@"feed"]];
            }
        } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } andLoadingViewOn:self.view];
}

- (IBAction)downVoteClicked:(id)sender
{
    [[OUAPICallManager manager] downVotePost:[[[feedArray objectAtIndex:currentFeed] valueForKey:@"post_id"] intValue] withSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess) {
        if (apiSuccess) {
            [self refreshCurrentFeed:[responseObject objectForKey:@"feed"]];
        }
    } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    } andLoadingViewOn:self.view];
    
}

- (IBAction)flagClicked:(id)sender
{
    [[OUAPICallManager manager] flagPost:[[[feedArray objectAtIndex:currentFeed] valueForKey:@"post_id"] intValue] withSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess) {
        if (apiSuccess) {
            [self refreshCurrentFeed:[responseObject objectForKey:@"feed"]];
        }
    } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    } andLoadingViewOn:self.view];

}

- (IBAction)postClicked:(id)sender
{
    if([postTextView.text length]>0)
    {
        [[OUAPICallManager manager] postStatus:postTextView.text WithSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess)
         {
            if(apiSuccess)
            {
                [feedArray insertObject:[responseObject objectForKey:@"feed"] atIndex:0];
                [feedScrollView reloadData];
                [self updateUI:[responseObject objectForKey:@"feed"]];
                [self toggleComposeVisibility];
                [self.view endEditing:TRUE];
            }
        } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } andLoadingViewOn:self.view];
    }
}

-(void)refreshCurrentFeed:(NSDictionary*)feedData
{
    [feedArray replaceObjectAtIndex:currentFeed withObject:feedData];
    [feedScrollView reloadData];
    [self updateUI:feedData];
}

-(void) textViewDidChange:(UITextView *)textView
{
    countLabel.text = [NSString stringWithFormat:@"%lu",140 - [textView.text length]];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (text.length==0) {
        return YES;
    }
    if (postTextView.text.length==140)
    {
        return NO;
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
