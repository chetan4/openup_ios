//
//  BaseViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 18/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)showMenu;
@end
