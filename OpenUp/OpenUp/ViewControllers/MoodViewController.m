//
//  MoodViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 28/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "MoodViewController.h"
#import "OUAPICallManager.h"
#import "AppDelegate.h"
#import "Utils.h"
@interface MoodViewController ()

@end

@implementation MoodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *minImage = [[UIImage imageNamed:@"progress_white"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIImage *maxImage = [[UIImage imageNamed:@"progress_orange"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
    UIImage *thumbImage = [UIImage imageNamed:@"mood_slider_knob"];
    
    [[UISlider appearance] setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbImage forState:UIControlStateNormal];
    
    [moodSlider addTarget:self action:@selector(moodChanged) forControlEvents:UIControlEventValueChanged];
    
    proceedButton.enabled = FALSE;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)moodChanged
{
    proceedButton.enabled = TRUE;
    currentMoodValue = moodSlider.value;
    if (currentMoodValue<=0) {
        currentMoodValue=1;
    }
    if (currentMoodValue<=25) {
        smileyImage.image = [UIImage imageNamed:@"very_sad"];
    }
    else if (currentMoodValue > 25 && currentMoodValue <= 50)
    {
        smileyImage.image = [UIImage imageNamed:@"sad"];
    }
    else if (currentMoodValue > 50 && currentMoodValue <= 75)
    {
        smileyImage.image = [UIImage imageNamed:@"happy"];
    }
    else
    {
        smileyImage.image = [UIImage imageNamed:@"very_happy"];
    }
    
}
-(IBAction)proceedClicked:(id)sender
{
    [[OUAPICallManager manager] updateMood:currentMoodValue withSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess)
    {
        if (apiSuccess) {
                [Utils updateMoodDateInDefaults];
                [self dismissViewControllerAnimated:TRUE completion:^{
                    
                }];
        }
    } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    } andLoadingViewOn:self.view];
}
@end
