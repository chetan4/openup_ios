//
//  RegisterViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 08/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "RegisterViewController.h"
#import "Utils.h"
#import "OUAPICallManager.h"
#import "VerifyPinViewController.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)registerClicked:(id)sender
{
    if ([Utils validateEmailWithString:emailTextField.text])
    {
        [self.view endEditing:TRUE];
        [[OUAPICallManager manager] registerUser:emailTextField.text WithSuccessHandler:^(AFHTTPRequestOperation *operation, id responseObject, bool apiSuccess) {
            if (apiSuccess)
            {
                VerifyPinViewController *verifyPinViewController = [[VerifyPinViewController alloc]initWithNibName:@"VerifyPinViewController" bundle:nil];
                verifyPinViewController.emailToVerify = emailTextField.text;
                [self.navigationController pushViewController:verifyPinViewController animated:YES];
            }
        } failureHandler:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } andLoadingViewOn:self.view];
    }
    else
    {
        
        [Utils showAlertWithTitle:NSLocalizedString(@"appName", nil) andMessage:NSLocalizedString(@"enter_valid_email", nil)];
    }
}
@end
