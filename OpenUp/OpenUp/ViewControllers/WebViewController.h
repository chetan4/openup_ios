//
//  WebViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 27/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface WebViewController : BaseViewController
{
    IBOutlet UIWebView *webView;
}

@property (nonatomic,strong) NSString *urlToLoad;
@property (nonatomic,strong) NSString *navTitle;
@end
