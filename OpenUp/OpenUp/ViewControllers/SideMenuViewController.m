//
//  SideMenuViewController.m
//  OpenUp
//
//  Created by Javal Nanda on 18/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import "SideMenuViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "HomeViewController.h"
#import "WebViewController.h"
#import "OUURLs.h"
#import "Utils.h"
#import "Constants.h"
#import "InviteViewController.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems = [NSArray arrayWithObjects:@"FEED",@"ABOUT OPEN UP",@"TERMS OF SERVICES",@"PRIVACY & ANONYMITY",@"COMMUNITY STANDARDS", nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    CGRect frame = CGRectMake(50, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height);
    
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = [UIColor lightGrayColor];
    label.text = [menuItems objectAtIndex:indexPath.row];
    [cell.contentView addSubview:label];
    
    return cell;
}

#pragma mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath{
    UINavigationController *navigationController;
    switch (indexPath.row) {
        case 0:
        {
            HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
            navigationController = [[UINavigationController alloc]initWithRootViewController:homeViewController];
        }
            break;
        case 1:
        {
            WebViewController *webController = [[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            navigationController = [[UINavigationController alloc]initWithRootViewController:webController];
            webController.urlToLoad = ABOUT_US;
            webController.navTitle = [[menuItems objectAtIndex:indexPath.row] uppercaseString];
        }
            break;
        case 2:
        {
            WebViewController *webController = [[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            navigationController = [[UINavigationController alloc]initWithRootViewController:webController];
            webController.urlToLoad = TERMS_OF_SERVICE;
            webController.navTitle = [[menuItems objectAtIndex:indexPath.row] uppercaseString];
        }
            
            break;
        case 3:
        {
            WebViewController *webController = [[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            navigationController = [[UINavigationController alloc]initWithRootViewController:webController];
            webController.urlToLoad = PRIVACY;
            webController.navTitle = [[menuItems objectAtIndex:indexPath.row] uppercaseString];
        }
            
            break;
        case 4:
        {
            WebViewController *webController = [[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
            navigationController = [[UINavigationController alloc]initWithRootViewController:webController];
            webController.urlToLoad = COMMUNITY_STANDARDS;
            webController.navTitle = [[menuItems objectAtIndex:indexPath.row] uppercaseString];
        }
            
            break;
        default:
            break;
    }
    
    [[UINavigationBar appearance]setBarTintColor:[Utils colorWithHexString:SECONDARY_COLOR]];
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,nil]];
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
}


-(IBAction)inviteClicked:(id)sender
{
    InviteViewController *inviteViewController = [[InviteViewController alloc] initWithNibName:@"InviteViewController" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:inviteViewController];
    [[UINavigationBar appearance]setBarTintColor:[Utils colorWithHexString:SECONDARY_COLOR]];
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,nil]];
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];

}

-(IBAction)deleteClicked:(id)sender
{

    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Delete Account" message:NSLocalizedString(@"delete_confirmation_message", nil) delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alertView.tag = 111;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==111)
    {
        if (buttonIndex==1)
        {
             //Remove user token
             [Utils writeToDefaultsForKey:USER_TOKEN_DEFAULTS andValue:@""];
             
             AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication]delegate];
             
             //Redirect to register screen
             RegisterViewController *rootViewController = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
             appDel.navigationController = [[UINavigationController alloc]initWithRootViewController:rootViewController];
             appDel.window.rootViewController = appDel.navigationController;
             [[UINavigationBar appearance]setBarTintColor:[Utils colorWithHexString:PRIMARY_COLOR]];
             appDel.navigationController.navigationBarHidden = TRUE;
        }
    }
}
@end
