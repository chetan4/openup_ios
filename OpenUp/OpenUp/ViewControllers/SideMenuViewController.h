//
//  SideMenuViewController.h
//  OpenUp
//
//  Created by Javal Nanda on 18/05/15.
//  Copyright (c) 2015 Javal Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
@interface SideMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *menuTable;
    NSArray *menuItems;
}
-(IBAction)inviteClicked:(id)sender;
-(IBAction)deleteClicked:(id)sender;
@end
